<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>Gathr | Dashboard</title>
    <link href="/assets/css/bootstrap.min.css" rel="stylesheet">
    <link href="/assets/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="/assets/css/animate.css" rel="stylesheet">
    <link href="/assets/css/plugins/iCheck/custom.css" rel="stylesheet">
    <link href="/assets/css/style.css" rel="stylesheet">
    <link href="/assets/css/plugins/steps/jquery.steps.css" rel="stylesheet">
    <link href="/assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
    <link href="/assets/css/plugins/daterangepicker/daterangepicker-bs3.css" rel="stylesheet">
    <link href="/assets/css/plugins/footable/footable.core.css" rel="stylesheet">

    <link href="/assets/css/plugins/summernote/summernote.css" rel="stylesheet">
    <link href="/assets/css/plugins/summernote/summernote-bs3.css" rel="stylesheet">

    <link href="/assets/css/plugins/chosen/chosen.css" rel="stylesheet">

    <link href="/assets/css/plugins/clockpicker/clockpicker.css" rel="stylesheet">

    

</head>

<body>

<div id="wrapper">

    @yield('side-nav')

    @yield('head-nav')

    @yield('content')

    
</div>

<!-- Mainly scripts -->
<script src="/assets/js/jquery-2.1.1.js"></script>
<script src="/assets/js/bootstrap.min.js"></script>
<script src="/assets/js/plugins/metisMenu/jquery.metisMenu.js"></script>
<script src="/assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>

<script src="/assets/js/plugins/staps/jquery.steps.min.js"></script>
<script src="/assets/js/plugins/validate/jquery.validate.min.js"></script>
<script src="/assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>


<!-- Custom and plugin javascript -->
<script src="/assets/js/script.js"></script>
<script src="/assets/js/inspinia.js"></script>
<script src="/assets/js/plugins/pace/pace.min.js"></script>


<!-- FootCustom and plugin javascript -->
<script src="/assets//js/plugins/footable/footable.all.min.js"></script>

    <!-- SUMMERNOTE -->
<script src="/assets/js/plugins/summernote/summernote.min.js"></script>

    <!-- Chosen -->
    <script src="/assets/js/plugins/chosen/chosen.jquery.js"></script>

        <!-- Clock picker -->
    <script src="/assets/js/plugins/clockpicker/clockpicker.js"></script>

   <script>
        $(document).ready(function(){


            $('.summernote').summernote();
         
            $('.chosen-select').chosen();

            $('.clockpicker').clockpicker();

            
           

            });
            var edit = function() {
                $('.click2edit').summernote({focus: true});
            };


                 $('.input-group.schedule').datepicker({
                    todayBtn: "linked",
                    keyboardNavigation: false,
                    forceParse: false,
                    calendarWeeks: true,
                    autoclose: true
                });

                $('.footable').footable();
                $('.footable2').footable();
           


          
  
    </script>




</body>

</html>
