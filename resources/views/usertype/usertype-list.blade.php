@extends('dashboard.dashboard-main')


@section('event-content')



    <div id="page-wrapper" class="gray-bg">
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                    <div class="col-lg-12">
                    @if (Session::has('status'))
                        <div class="alert alert-success">{{Session::get('status')}}</div>
                    @endif

                    <div class="ibox-content">
                                 <table class="footable table table-stripped toggle-arrow-tiny" data-page-size="8">
                                <thead>
                                <tr>

                                    <th data-toggle="true">Event Name</th>
                                    <th>Event Details</th>
                                    <th>Event Location</th>
                                    <th data-hide="all">Venue Name</th>
                                    <th data-hide="all">Start Date</th>
                                    <th data-hide="all">End Date</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>

                                @foreach ($user['events'] as $event)
                                <tr>
                                    <td>{{$event['event_name']}}</td>
                                    <td>{{$event['event_details']}}</td>
                                    <td>{{$event['event_location']}}</td>
                                    <td>{{$event['venue_name']}}</td>
                                    <td>{{$event['start_date']}}</td>
                                    <td>{{$event['end_date']}}</td>
                                    <td>{{$event['event_name']}}</td>
                                    <td><a href="#"><i class="fa fa-check text-navy"></i></a></td>
                                </tr>
                                    
                                @endforeach
               
                             
                        
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="5">
                                        <ul class="pagination pull-right"></ul>
                                    </td>
                                </tr>
                                </tfoot>
                            </table>
                            </div>
                            </div>
                        </div>

                  
               
                    </div>
                </div>
            </div>
        </div>
        <div class="footer">
       
            <div>
                <strong>Copyright</strong> Gathr &copy; 2014-2015
            </div>
        </div>

    </div>


@endsection
