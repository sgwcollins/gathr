@extends('dashboard.dashboard-main')


@section('event-content')



    <div id="page-wrapper" class="gray-bg">
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                    <div class="col-lg-12">
                    @if (Session::has('status'))
                        <div class="alert alert-success">{{Session::get('status')}}</div>
                    @endif
                        <div class="ibox">
                            <div class="ibox-title">
                                <h5>Event Creation</h5>
                                <div class="ibox-tools">
                                  
                                
                               
                                </div>
                            </div>
                            <div class="ibox-content">
                                <h2>
                                    Create your event
                                </h2>
                           

                                <form id="form" method="POST" action="/event/create">
                                {!! csrf_field() !!} 
                                    <fieldset>
                                        <div class="row">
                                            <div class="col-lg-8">
                                                <div class="form-group">
                                                    <label>Event Name</label>
                                                    <input id="eventName" name="eventName" type="text" class="form-control required">
                                                </div>
                                                <div class="form-group">
                                                    <label>Event Detail</label>
                                                    <input id="eventDetail" name="eventDetail" type="text" class="form-control required">
                                                </div>
                                                <div class="form-group">
                                                    <label>Event Location</label>
                                                    <input id="eventLocation" name="eventLocation" type="text" class="form-control required">
                                                </div>
                                                <div class="form-group">
                                                    <label>Venue Name</label>
                                                    <input id="venueName" name="confirm" type="text" class="form-control required">
                                                </div>
                                                <div class="form-group" id="data_1">
                                                    <label class="font-noraml">Start Date</label>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="startDate" class="form-control" value="03/04/2014">
                                                    </div>
                                                </div>
                                                <div class="form-group" id="data_1">
                                                    <label class="font-noraml">End Date</label>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span><input type="text" name="endDate"  class="form-control" value="03/04/2014">
                                                    </div>
                                                </div>


                                                <button type="submit" class="btn btn-primary block full-width m-b">Submit</button>

                                            </div>
                                            <div class="col-lg-4">
                                                <div class="text-center">
                                                    <div style="margin-top: 20px">
                                                        <i class="fa fa-sign-in" style="font-size: 180px;color: #e5e5e5 "></i>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                </form>
                            </div>
                        </div>
                        </div>

                  
               
                    </div>
                </div>
            </div>
        </div>
        <div class="footer">
       
            <div>
                <strong>Copyright</strong> Gathr &copy; 2014-2015
            </div>
        </div>

    </div>


@endsection
