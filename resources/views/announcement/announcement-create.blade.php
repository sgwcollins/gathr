@extends('dashboard.dashboard-main')


@section('event-content')
 
    <div id="page-wrapper" class="gray-bg">
        <div class="wrapper wrapper-content animated fadeInRight">
            <div class="row">
                    <div class="col-lg-12">
                    @if (Session::has('status'))
                        <div class="alert alert-success">{{Session::get('status')}}</div>
                    @endif
                        <div class="ibox">
                            <div class="ibox-title">
                                <h5>Announcement Creation</h5>
                                <div class="ibox-tools">
                                  
                                
                               
                                </div>
                            </div>

                               <div class="mail-box">


                <div class="mail-body">

                    <form class="form-horizontal" method="post">


                        <div class="form-group"><label class="col-sm-2 control-label">Title:</label>

                            <div class="col-sm-10"><input type="text" class="form-control" value=""></div>
                        </div>

                    	 <div class="form-group"><label class="col-sm-2 control-label">Schedule for:</label>

                        <div class="col-sm-10">
                        	<div class="input-group date schedule pull-left">
                            	<span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                            	<input type="text" class="form-control" value="07/01/2014">
                        	</div>
                        	<div class="input-group clockpicker pull-right" data-autoclose="true">
                                <input type="text" class="form-control" value="09:30">
                                <span class="input-group-addon">
                                    <span class="fa fa-clock-o"></span>
                                </span>
                            </div>
                        </div>

                        

                        </div>

                        <div class="form-group"><label class="col-sm-2 control-label">Event</label>

	                        <div class="col-sm-10">
	                         <div class="input-group">
				                <select data-placeholder="Choose a Country..." class="chosen-select" style="width:350px;" tabindex="2">
					                <option value="">Select</option>

					                @foreach ($user['events'] as $event)
					                
					                	<option value="{{$event['id']}}">{{$event['event_name']}}</option>
		

					                @endforeach
					            </select>
	                        </div>

                        </div>
                        </div>
                            <div class="form-group"><label class="col-sm-2 control-label">User group:</label>

                            <div class="col-sm-10">

                            		<select data-placeholder="Choose a usergroup" class="usergroup chosen-select" style="width:350px;" tabindex="2">
					                <option value="">Select</option>

					                @foreach ($user['events'] as $event)

					                
					                	<option value="{{$event['id']}}">{{$event['event_name']}}</option>
		

					                @endforeach
					            	</select>
                            </div>
                        </div>
                        <input type="hidden" name="announcement" id="announcement">
                        </form>

                </div>

                    <div class="mail-text h-200">

                        <div class="summernote">
                            <h3>Hello Jonathan! </h3>
                            dummy text of the printing and typesetting industry. <strong>Lorem Ipsum has been the industry's</strong> standard dummy text ever since the 1500s,
                            when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic
                            typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with
                            <br/>
                            <br/>

                        </div>
<div class="clearfix"></div>
                        </div>
                    <div class="mail-body text-right tooltip-demo">
                        <a href="#" class="btn btn-sm btn-primary 	" data-toggle="tooltip" data-placement="top" title="Send">
                        	<i class="fa fa-reply"></i> 
                    		Send
                        </a>
                        <a href="mailbox.html" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Discard email"><i class="fa fa-times"></i> Discard</a>
                        <a href="mailbox.html" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Move to draft folder"><i class="fa fa-pencil"></i> Draft</a>
                    </div>
                    <div class="clearfix"></div>



                </div>
                        </div>
                        </div>

                  
               
                    </div>
                </div>
            </div>
        </div>
        <div class="footer">
       
            <div>
                <strong>Copyright</strong> Gathr &copy; 2014-2015
            </div>
        </div>

    </div>


@endsection
