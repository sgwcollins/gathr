    @extends('layouts.master-login')
@section('register')
@parent
<link href="/assets/css/plugins/iCheck/custom.css" rel="stylesheet">
<link href="/assets/css/plugins/datapicker/datepicker3.css" rel="stylesheet">
<div class="middle-box text-center loginscreen animated fadeInDown">
    <div>
        <div>
            <img class="logo-image" src="/assets/images/logo/gathr.png">
        </div>

        <h3>Register to Gathr </h3>
        <p>Create account to see it in action.</p>
        <form class="m-t" role="form" method="POST" action="/auth/register">
            {{-- Prevent cross browser hacking--}}
            {!! csrf_field() !!}
            <div class="form-group">
            @if( ! empty($user))
               <input type="text" class="form-control" placeholder="Username"  name="username" value="
                {{$user->getNickName()}}">

                <div class="form-group">
                    <input type="email" class="form-control" placeholder="Email" type="email" name="email" value="{{$user->getEmail()}}">
                </div>
            @else
            <div class="form-group">
                <input type="text" class="form-control" placeholder="Username"  name="username" value="">
            </div>

                <div class="form-group">
                    <input type="email" class="form-control" placeholder="Email" type="email" name="email" value="">
                </div>

            @endif
             
            </div>
 
            <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="Password" >
            </div>
            <div class="form-group">
                <input type="password" name="password_confirmation" class="form-control" placeholder="Confirm Password">
            </div>
   

       
            
            <button type="submit" class="btn btn-primary block full-width m-b">Register</button>
            <p class="text-muted text-center"><small>Already have an account?</small></p>
            <a class="btn btn-sm btn-white btn-block" href="/">Login</a>
            <input type="hidden" name="_token" value="{{ csrf_token() }}">
        </form>
        @if (count($errors) > 0)
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li> {{ $error }} </li>
                @endforeach
            </ul>
        </div>
        @endif
        <p class="m-t"> <small>Inspinia we app framework base on Bootstrap 3 &copy; 2014</small> </p>
    </div>
</div>
<script src="assets/js/plugins/iCheck/icheck.min.js"></script>
<script src="assets/js/plugins/datapicker/bootstrap-datepicker.js"></script>
<script>
$(document).ready(function(){
$('.i-checks').iCheck({
checkboxClass: 'icheckbox_square-green',
radioClass: 'iradio_square-green',
});
$('#data_3 .input-group.date').datepicker({
startView: 2,
todayBtn: "linked",
keyboardNavigation: false,
forceParse: false,
autoclose: true
});
$('#data_3 .input-group.date').datepicker({
startView: 2,
todayBtn: "linked",
keyboardNavigation: false,
forceParse: false,
autoclose: true
});
$( ".fa-map-marker.country" ).click(function(){
$(this).addClass('hide')
$('.country-spinner').removeClass('hide');
// Try HTML5 geolocation.
if (navigator.geolocation) {
navigator.geolocation.getCurrentPosition(function(position) {
var pos = {
lat: position.coords.latitude,
lng: position.coords.longitude
};
$.getJSON( "http://maps.googleapis.com/maps/api/geocode/json?latlng="+pos['lat']+","+pos['lng']+"&sensor=true)", function( data ) {
$(".country").val(data['results'][7]['formatted_address']);
$('.fa-map-marker.country').removeClass('hide')
$('.country-spinner').addClass('hide');
});
}, function() {
handleLocationError(true, infoWindow, map.getCenter());
});
} else {
// Browser doesn't support Geolocation
handleLocationError(false, infoWindow, map.getCenter());
}
function handleLocationError(browserHasGeolocation, infoWindow, pos) {
infoWindow.setPosition(pos);
infoWindow.setContent(browserHasGeolocation ?
'Error: The Geolocation service failed.' :
'Error: Your browser doesn\'t support geolocation.');
}
});
$( ".fa-map-marker.address" ).click(function(){
$(this).addClass('hide')
$('.address-spinner').removeClass('hide');
// Try HTML5 geolocation.
if (navigator.geolocation) {
navigator.geolocation.getCurrentPosition(function(position) {
var pos = {
lat: position.coords.latitude,
lng: position.coords.longitude
};
$.getJSON( "http://maps.googleapis.com/maps/api/geocode/json?latlng="+pos['lat']+","+pos['lng']+"&sensor=true)", function( data ) {
$(".address").val(data['results'][0]['formatted_address']);
$('.fa-map-marker.address').removeClass('hide')
$('.address-spinner').addClass('hide');

});
}, function() {
handleLocationError(true, infoWindow, map.getCenter());
});
} else {
// Browser doesn't support Geolocation
handleLocationError(false, infoWindow, map.getCenter());
}
function handleLocationError(browserHasGeolocation, infoWindow, pos) {
infoWindow.setPosition(pos);
infoWindow.setContent(browserHasGeolocation ?
'Error: The Geolocation service failed.' :
'Error: Your browser doesn\'t support geolocation.');
}
});
});
</script>
@endsection