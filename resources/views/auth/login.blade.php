@extends('layouts.master-login')
<div class="middle-box text-center loginscreen animated fadeInDown">
@if (Session::has('flash_message'))
    <div class="alert alert-success">{{Session::get('flash_message')}}</div>
@endif
@if (Session::has('flash_error'))
    <div class="alert alert-danger">{{Session::get('flash_error')}}</div>
@endif


    <div>
        <div>
            <img class="logo-image" src="/assets/images/logo/gathr.png">
        </div>
        <h3>Welcome to Gathr</h3>
     
        </p>
        <p>Login in. To see it in action.</p>

        @if (count($errors) > 0)
            <div class="alert alert-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li> {{ $error }} </li>
                    @endforeach
                </ul>
            </div>
        @endif
        <form class="m-t" role="form" method="POST" action="/auth/login">
            {!! csrf_field() !!}   
            <div class="form-group">
                <input  type="email" name="email" class="form-control" placeholder="Username" value="{{ old('email') }}" >
            </div>
            <div class="form-group">
                <input type="password" name="password" class="form-control" placeholder="Password">
            </div>
            <div class="form-group">
                <input type="checkbox" name="remember"> Remember Me
            </div>
            <button type="submit" class="btn btn-primary block full-width m-b">Login</button>
            <a href="#"><small>Forgot password?</small></a>
            <p class="text-muted text-center"><small>Do not have an account?</small></p>
            <a class="btn btn-sm btn-white btn-block" href="/auth/register">Create an account</a>
        </form>

        <div class="social">

        <a class="btn btn-block btn-social btn-facebook" href="/auth/facebook">
            <i class="fa fa-facebook"></i>
            Login with Facebook
        </a>
        <a class="btn btn-block btn-social btn-twitter" href="/auth/twitter">
            <i class="fa fa-twitter"></i>
            Login with Twitter
        </a>
        <a class="btn btn-block btn-social btn-linkedin" href="/auth/linkedin">
            <i class="fa fa-linkedin"></i>
            Login with Linkedin
        </a>
        <a class="btn btn-block btn-social btn-google" href="/auth/google">
            <i class="fa fa-google"></i>
            Login with Google
        </a>


  


        </div>





        
    </div>
</div>