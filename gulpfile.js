var gulp = require('gulp'); 
//var elixir = require('laravel-elixir');
var sass = require('gulp-sass');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

//elixir(function(mix) {
//    mix.sass('app.scss');
//});


gulp.task('sass', function() {
    gulp.src('public/assets/sass/**/*.scss')
    .pipe(sass())
    .pipe(gulp.dest('public/assets/css/'));
});

// Watch Files For Changes
gulp.task('watch', function() {
 
    gulp.watch('public/assets/sass/**/*.scss', ['sass']);
});


// Default Task
gulp.task('default', ['sass' ,'watch']);