<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */


    'mandrill' => [
        'secret' => env('MANDRILL_SECRET'),
    ],

    'github' => [
        'client_id' => 'dcd06c863431a2ef9d46',
        'client_secret' => '37e503664e19405200d2b7895a79ecec6280dbe1',
        'redirect' => 'http://stage.gathrhq.com/auth/callback/github',
    ],
    'facebook' => [
        'client_id' => '1490254971280435',
        'client_secret' => 'd1aab8ba55ca4b591343495877ae981f',
        'redirect' => 'http://stage.gathrhq.com/auth/callback/facebook',
    ],
    'twitter' => [
        'client_id' => 'DAZ2UfHXLMbFt68HOseQpcVLl',
        'client_secret' => 'bCTujVyik7OZ245PtBAh1u1eWkkCRehOokJzmXf3DXA5GYu7VE',
        'redirect' => 'http://stage.gathrhq.com/auth/callback/twitter',
    ],
    'linkedin' => [
        'client_id' => '77tbl88t4kiwta',
        'client_secret' => 'QfXu1GC87too4kcj',
        'redirect' => 'http://stage.gathrhq.com/auth/callback/linkedin',
    ],
    'google' => [
        'client_id' => '518670050320-49vb54l8s56ba355sb4cjbvth83ad1u2.apps.googleusercontent.com',
        'client_secret' => 'QoQYnqaUVUGjeOkxQkS4VWCW',
        'redirect' => 'http://stage.gathrhq.com/auth/callback/google',
    ],



];
