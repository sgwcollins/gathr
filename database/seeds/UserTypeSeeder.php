<?php

use Illuminate\Database\Seeder;

class UserTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('usertype')->insert([
            'usertype_name' => "Admin"
        ]);
        DB::table('usertype')->insert([
            'usertype_name' => "Attende"
        ]);
        DB::table('usertype')->insert([
            'usertype_name' => "Promoter"
        ]);
    }
}
