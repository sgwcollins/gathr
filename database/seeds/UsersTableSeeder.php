<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       factory(App\User::class, 1)->create([
            'username' => 'sydney',
            'email' => 'sgwcollins@gmail.com',
        	'password' => bcrypt('random123')
        ]);
    }
}
