<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');

            
            $table->string('username')->unique();
            $table->string('email')->unique()->nullable();
            $table->string('password', 60)->nullable();
            $table->string('address')->nullable();
            $table->string('first_name')->nullable();;
            $table->string('last_name')->nullable();
            $table->string('phone')->nullable();;
            $table->date('DOB')->nullable();;
            $table->boolean('private')->nullable();
            $table->string('avatar')->default('assets/images/avatars/default_avatar.png')->nullable();
            $table->string('confirmation_code')->nullable();
            $table->boolean('verified')->default(false);


            $table->string('socialtype')->default('none');
            $table->string('social_id')->nullable();




            $table->rememberToken();
            $table->timestamps();
     
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('users');
    }
}
