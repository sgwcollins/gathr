<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;


class UserType extends Model 
                                    
                                    
{

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'usertype';


    public function events()
    {
        return $this->belongsToMany('App\Event');
    }




}
