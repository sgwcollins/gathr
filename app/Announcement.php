<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Announcement extends Model
{

	protected $table = 'announcement';

    public function users()
    {
        return $this->belongsToMany('App\User');
    }
    
    public function events()
    {
        return $this->belongsTo('App\Event');
    }

    public function userType(){
    	
    	return $this->belongsTo('App\UserType');
    }
}
