<?php

namespace App\Http\Controllers\Auth;

use App\User;
use Validator;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Socialite;
use Illuminate\Support\Facades\DB;
use Auth;
use Mail;
use Session;
use Redirect;

class AuthController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'username' => 'required|max:255|unique:users',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return User
     */
    protected function create(array $data)
    {

        $confirmation_code = str_random(30);


        

        $data_confirm = ['confirmation_code' => $confirmation_code];

        // Mail::send('email.verify-email', $data_confirm, function($message) use ($data) {
        //     $message->to($data['email'], $data['username'])
        //         ->subject('Verify your email address');
        // });

        return User::create([
            'username' => $data['username'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
            'confirmation_code' => $confirmation_code
        ]);


    }

    /**
     * Redirect veridication email
     *
     * @return Response
     */
    public function verifyAccount($code)
    {
          $users = DB::table('users')
                ->where('confirmation_code', '=', $code)
                ->first();

            if ( empty($users) ) {

                    Session::flash('flash_error' ,'Your account could not be verified');
                    return Redirect::route('login');
            } else {


                        DB::table('users')
                        ->where('id', $users->id)
                        ->update(['verified' => true]);


                        Session::flash('flash_message' ,'Your account has been verified!');

                        if(Auth::check()){
                            return Redirect::route('home');
                        }else{
                            return Redirect::route('login');
                        }
                
            }

    }

    
    /**
     * Redirect the user to the GitHub authentication page.
     *
     * @return Response
     */
    public function redirectToProvider($social_type)
    {
        return Socialite::driver($social_type)->redirect();
    }

    /**
     * Obtain the user information from GitHub.
     *
     * @return Response
     */
    public function handleProviderCallback($social_type)
    {
        $user = Socialite::driver($social_type)->user();

        // OAuth Two Providers
        //$token = $user->token;

        // OAuth One Providers
        //$token = $user->token;
        //$tokenSecret = $user->tokenSecret;

        // All Providers
        $user->getId();

        $users = DB::table('users')
            ->where('email', '=', $user->getEmail() )
            ->first();

        if ( empty($users)) {
        
   

            //Auth::loginUsingId($id);
            return view('auth.register', ['user' => $user]);


        } else {

            Auth::loginUsingId($users->id);

            return Redirect::route('home');
            
                
        }


   




  

    }



}
