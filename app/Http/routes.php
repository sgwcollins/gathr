<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', ['as' => 'login' , function (){

  //   $data = [];

  // Mail::send('email.verify-email', $data , function($message) {
  //       $message->to('sgwcollins@gmail.com', 'Sydney')
  //           ->subject('Verify your email address');
  //   });



    return view('auth.login');


}]);

Route::get('home' , ['as' => 'home', function () {


	if (Auth::check()) {
    
      $data = Auth::user();

    	return view('event.event-create', ['user' => $data ,'page' => 'default']);
	}
  
}]);


Route::get('register',['as' => 'home', function(){

   return view('auth.register');

}]);

Route::get('email', function(){

	 return view('email.verfiy-email');

});


Route::get('verify/{confirmation_code}', 'Auth\AuthController@verifyAccount');



// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
Route::get('auth/register', 'Auth\AuthController@getRegister');
Route::post('auth/register', 'Auth\AuthController@postRegister');

Route::get('auth/{social_type}', 'Auth\AuthController@redirectToProvider');
Route::get('auth/callback/{social_type}', 'Auth\AuthController@handleProviderCallback');

//Event routes

Route::get('event/create', ['as' => 'event/create' , function (){

    if (Auth::check()) {
      
        $data = Auth::user();

        return view('event.event-create', ['user' => $data ,'page' => 'default']);
    }

}]);

Route::get('announcement/create', ['as' => 'announcement/create' , function (){

    if (Auth::check()) {
      
        $data = Auth::user();
        
        return view('announcement.announcement-create', ['user' => $data ,'page' => 'default']);
    }
}]);

Route::post('announcement/create', 'Auth\AuthController@postRegister');

Route::get('event/list', ['as' => 'event/create' , function (){

    if (Auth::check()) {
      
        $data = Auth::user();

        return view('event.event-list', ['user' => $data ,'page' => 'default']);
    }

}]);

Route::get('event/dashboard', ['as' => 'event/dashboard' , function (){

    if (Auth::check()) {
      
        $data = Auth::user();

        return view('event.event-list', ['user' => $data ,'page' => 'dashboard']);
    }

}]);


  
Route::post('event/create', 'EventController@createEvent');
//




