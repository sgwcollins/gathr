<?php

namespace App\Mailer;

use Illuminate\Contracts\Mail\Mailer;
use App\User;



Class AppMailer{

	protected $from = "info@gathr.com";

	protected $to;

	protected $view;

	protected $data  = [];

	protected $mailer;

	public function __construct(Mailer $mailer){

		$this->mailer = $mailer;

	}

	public function sendConfirmationEmail(User $user){
		$this->to 	=  $user->email;
		$this->view = 'email.verfiy-email';
		$this->data =  compact('user'); 

	}

	public function deliver(){
		
		$this->mailer->send($this->view , $this->data , function($message){
			$message->from($this->from, "Admistrator")
				->to($this->to);

		});
	}



}